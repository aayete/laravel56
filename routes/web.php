
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('roles', 'RoleController');
Route::resource('users', 'UserController');
Route::resource('spaces', 'SpaceController');
Route::resource('books', 'BookController');
// Route::resource('api/users', 'API\UserController');
Route::resource('api/roles', 'API\RoleController');
Route::resource('api/spaces', 'API\SpaceController');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/wish/{id}/book', 'WishController@wish');
Route::get('/wish', 'WishController@index');
Route::get('/wish/{id}/book', 'WishController@wishout');
Auth::routes();
Route::get('/redirect/google', 'SocialAuthController@redirect');
Route::get('/callback/google', 'SocialAuthController@callback');
Route::get('auth/github', 'Auth\AuthController@redirectToProvider');
Route::get('auth/github/callback', 'Auth\AuthController@handleProviderCallback');
Route::get('/mail/basket', 'MailController@basket');
Route::get('generate-pdf', 'PdfGenerateController@pdfview')->name('generate-pdf');
