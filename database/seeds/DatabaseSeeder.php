<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SpacesTableSeeder::class);
        $this->call(BooksTableSeeder::class);
        $this->call(BorrowsTableSeeder::class);
        $this->call(BooksUsersTableSeeder::class);
    }
}
