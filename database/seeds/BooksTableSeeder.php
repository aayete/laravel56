<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            'name' => 'Locos',
            'author' => 'R.R.Martin',
            'space_id' => 1,
        ]);
        DB::table('books')->insert([
            'name' => 'Locos2',
            'author' => 'R.R.Martin',
            'space_id' => 1,
        ]);
        DB::table('books')->insert([
            'name' => 'Don Quijote',
            'author' => 'Nos',
            'space_id' => 2,
        ]);
        DB::table('books')->insert([
            'name' => 'IT',
            'author' => 'SK',
            'space_id' => 1,
        ]);
        DB::table('books')->insert([
            'name' => 'Alice',
            'author' => 'Nolen',
            'space_id' => 3,
        ]);
        factory(App\Book::class, 15)->create();
    }
}
