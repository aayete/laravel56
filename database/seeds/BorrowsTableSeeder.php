<?php

use Illuminate\Database\Seeder;

class BorrowsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('borrows')->insert([
            'id' => 1,
            'user_id' => 1,
            'book_id' => 1
        ]);
        DB::table('borrows')->insert([
            'id' => 2,
            'user_id' => 1,
            'book_id' => 2
        ]);
        DB::table('borrows')->insert([
            'id' => 3,
            'user_id' => 2,
            'book_id' => 1
        ]);
        DB::table('borrows')->insert([
            'id' => 4,
            'user_id' => 2,
            'book_id' => 3
        ]);
        DB::table('borrows')->insert([
            'id' => 5,
            'user_id' => 1,
            'book_id' => 4
        ]);
    }
}
