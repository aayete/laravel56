<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'name' => $faker->word(10),
        'author' => $faker->name,
        'space_id' => rand(1, 4),
    ];
});
