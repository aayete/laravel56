<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use \App\Role;
use Auth;

class WishController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user = Auth::user();
        return view('wish/index', ['books' => $user->books]);
    }
    public function wish(Request $request, $id)
    {
        $user = User::find($id);
        $user->books()->syncWithoutDetaching($request->input('id'));
        return redirect('/wish');
    }
    public function wishout($id)
    {
        $user = Auth::user();
        $user->books()->detach($id);
        return redirect('/wish');
    }
}
