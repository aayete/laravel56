<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use PDF;
use \App\User;
use \App\Role;
use \App\Book;
use Auth;

class PdfGenerateController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function pdfview(Request $request)
    {
        // $books = \App\Book::all();
        // view()->share('books',$books);
        $user = Auth::user();
        view()->share('books', $user->books);

        if($request->has('download')){
            // Set extra option
            PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
            // pass view file
            $pdf = PDF::loadView('pdfs.index');
        dd($pdf);
            // download pdfpdf
            return $pdf->download('pdfview.pdf');
        }
        return view('pdfs.index');
    }
}
