<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\OrderBook;
use Mail;

class MailController extends Controller
{
    public function basket(Request $request)
    {
        $mermer = "Lista de deseos";
        Mail::send(new OrderBook($mermer));
    }
}
