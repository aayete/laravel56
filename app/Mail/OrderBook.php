<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use \App\User;
use \App\Role;
use \App\Book;
use Auth;
use PDF;

class OrderBook extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($basket)
    {
        $this->basket = $basket;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = Auth::user();
        view()->share('books', $user->books);
        $pdf = PDF::loadView('pdfs.index');
        return $this->to("mermer@gmail.com")->from("mermerino@gmailareno.com")->view('mail.books', ['books' => $user->borrows])->attachData($pdf->output(), 'filename.pdf');
    }
}
