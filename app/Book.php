<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'author', 'space_id',
    ];

    public function space()
    {
        return $this->belongsTo('App\Space', 'space_id');
    }

    public function borrow()
    {
        return $this->hasOne('App\Borrow');
    }
    public function users()
    {
        return $this->belongsToMany('App\User', 'books_users')
          ->withTimestamps();
    }
}
