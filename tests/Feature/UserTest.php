<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
       $this->get('/users')
            ->assertStatus(200)
            ->assertSee('ejem1@gmail.com');

        $this->get('/users')
            ->assertStatus(200)
            ->assertSee('ejem2@gmail.com');
            $this->get('/users')
            ->assertStatus(200)
            ->assertSee('ejem1');
            $this->get('/users')
            ->assertStatus(200)
            ->assertSee('ejem2');
            $this->get('/users')
            ->assertStatus(200)
            ->assertSee('1');
            $this->get('/users')
            ->assertStatus(200)
            ->assertSee('2');


    }
}
