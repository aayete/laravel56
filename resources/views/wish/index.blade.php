@extends('layouts.app')

@section('content')
<div class="container">

<h1>Lista de libros deseados</h1>

<table class="table table-bordered">
    <tr>
        <th>Nombre</th>
        <th>Autor</th>
        <th>Opciones</th>
    </tr>
@foreach ($books as $book)
<tr>
    <td>{{ $book->name }}</td>
    <td>{{ $book->author }}</td>
    <td><a href="/wish/{{$book->id}}/book">Quitar</a></td>
</tr>
@endforeach
</table>

<a href="{{ route('generate-pdf',['download'=>'pdf']) }}">Download PDF</a>

</div>
@endsection

