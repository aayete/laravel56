@extends('layouts.app')

@section('content')

<div class="container">

<h1>Edicion de libro</h1>

<form method="post" action="/books/{{ $book->id }}">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">

     <div  class="form-group">
        <label>Nombre</label>
        <input class="form-control"  type="text" name="name" value="{{ $book->name }}">
        @if ($errors->first('name'))
            <div class="alert alert-danger">
                {{ $errors->first('name') }}
            </div>
        @endif
    </div>

    <div  class="form-group">
        <label>Autor</label>
        <input class="form-control"  type="text" name="author" value="{{ $book->author }}">
        @if ($errors->first('author'))
            <div class="alert alert-danger">
                {{ $errors->first('author') }}
            </div>
        @endif
    </div>

    <div  class="form-group">
        <label>Localizacion</label>
        <select class="form-control" name="space_id">
            @foreach ($locations as $location)
                @if($book->space_id == $location->id)
                    <option selected="selected" value="{{$location->id}}">{{$location->location}}</option>
                @else
                    <option value="{{$location->id}}">{{$location->location}}</option>
                @endif
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label></label>
        <input class="form-control"  type="submit" name="" value="Nuevo">
    </div>

</form>

</div>
@endsection
