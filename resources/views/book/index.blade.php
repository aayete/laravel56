@extends('layouts.app')

@section('content')
<div class="container">

<h1>Lista de Libros</h1>

    <table class="table table-bordered">
    <tr>
        <th>Nombre</th>
        <th>Autor</th>
        <th>Localizacion</th>
        <th>Opciones</th>
        <th>Añadir a lista</th>
    </tr>
    @foreach ($books as $book)
    <tr>
        <td>{{ $book->name }}</td>
        <td>{{ $book->author }}</td>
        <td>{{ $book->space->location }}</td>
        <td>

            <form method="post" action="/books/{{ $book->id }}">

            @can ('view', $book)
            <a class="btn btn-info" href="/books/{{ $book->id }}">Ver</a>
            @endcan
            @can ('update', $book)
            <a class="btn btn-info" href="/books/{{ $book->id }}/edit">Editar</a>
            @endcan
            @can ('delete', $book)
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input class="btn btn-danger" type="submit" value="borrar">
            @endcan
            </form>
        </td>
        <td>
            <form method="post" action="/wish/{{ Auth::user()->id }}/book">
                {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $book->id }}">
                <input type="checkbox" name="libros[]">
                <input type="submit" value="añadir">
            </form>
        </td>
    </tr>
    @endforeach
</table>

@can ('create', App\Book::class)
<a href="/books/create" class="btn btn-info">Nuevo</a>
@endcan
{{ $books->links() }}
</div>
@endsection
