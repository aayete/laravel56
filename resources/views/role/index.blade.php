@extends('layouts.app')

@section('content')
<div class="container">

<h1>Lista de Usuarios</h1>

<table class="table table-bordered">
    <tr>
        <th>Nombre</th>
    </tr>
@foreach ($roles as $role)
<tr>
    <td>{{ $role->name }}</td>
</tr>
@endforeach
</table>
</div>
@endsection
