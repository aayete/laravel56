@extends('layouts.app')

@section('content')

<div class="container">

<h1>Edicion de espacio</h1>

<form method="post" action="/spaces/{{ $space->id }}">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">

    <div  class="form-group">
        <label>Localizacion</label>
        <input class="form-control"  type="text" name="location" value="{{ $space->location }}">
        @if ($errors->first('location'))
            <div class="alert alert-danger">
                {{ $errors->first('location') }}
            </div>
        @endif
    </div>

    <div class="form-group">
        <label></label>
        <input class="form-control"  type="submit" name="" value="Nuevo">
    </div>

</form>

</div>
@endsection
