@extends('layouts.app')

@section('content')
<div class="container">

<h1>Crear espacio</h1>

<form method="post" action="/spaces">
    {{ csrf_field() }}

    <div  class="form-group">
        <label>Localizacion</label>
        <input class="form-control"  type="text" name="location" value="{{ old('location') }}>
        @if ($errors->first('location'))
            <div class="alert alert-danger">
                {{ $errors->first('location') }}
            </div>
        @endif
    </div>

    <div class="form-group">
        <label></label>
        <input class="form-control"  type="submit" name="" value="Nuevo">
    </div>



</form>

</div>
@endsection
